﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.IO;
using System.Reflection;

namespace CoolParking.Console
{
    class Program
    {
        private static ParkingService _parkingService;
        static void Main(string[] args)
        {
            _parkingService = new ParkingService(
                new TimerService(Settings.PaymentPeriodSeconds * 1000),
                new TimerService(Settings.LoggingPeriodSeconds * 1000),
                new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log"));

            System.Console.WriteLine($"Parking start work: {_parkingService.StartWork}");
            int userCommand = 0;
            while (true)
            {
                try
                {
                    System.Console.WriteLine();
                    System.Console.WriteLine("1. Вивести на екран поточний баланс Паркiнгу.");
                    System.Console.WriteLine("2. Вивести на екран суму зароблених коштiв за поточний перiод(до запису у лог).");
                    System.Console.WriteLine("3. Вивести на екран кiлькiсть вiльних мiсць на паркуваннi(вiльно X з Y).");
                    System.Console.WriteLine("4. Вивести на екран усi Транзакцiї Паркiнгу за поточний перiод(до запису у лог).");
                    System.Console.WriteLine("5. Вивести на екран iсторiю Транзакцiй(зчитавши данi з файлу Transactions.log).");
                    System.Console.WriteLine("6. Вивести на екран список Тр.засобiв, що знаходяться на Паркiнгу.");
                    System.Console.WriteLine("7. Поставити Транспортний засiб на Паркiнг.");
                    System.Console.WriteLine("8. Забрати Транспортний засiб з Паркiнгу.");
                    System.Console.WriteLine("9. Поповнити баланс конкретного Тр.засобу.\n");

                    System.Console.Write("Введiть номер команди: ");
                    int.TryParse(System.Console.ReadLine(), out userCommand);

                    switch (userCommand)
                    {
                        case 1: System.Console.WriteLine(_parkingService.GetBalance()); break;
                        case 2: System.Console.WriteLine(_parkingService.EarnedMoney()); break;
                        case 3: System.Console.WriteLine($"{_parkingService.GetFreePlaces()} з {Settings.StartingParkingCapacity}"); break;
                        case 4: _parkingService.ListOfTransactions().ForEach(t => System.Console.WriteLine(t.ToString())); break;
                        case 5: System.Console.WriteLine(_parkingService.ReadFromLog()); break;
                        case 6: _parkingService.ListOfVehicles().ForEach(v => System.Console.WriteLine(v.ToString())); break;
                        case 7: AddVechile(); break;
                        case 8: RemoveVechile(); break;
                        case 9: System.Console.WriteLine(_parkingService.GetBalance()); break;
                        default:
                            System.Console.WriteLine("Невiрний номер команди.");
                            break;
                    }
                }
                catch (Exception e)
                {
                    System.Console.WriteLine(e.Message);
                }
            }
        }

        public static void AddVechile()
        {
            System.Console.Write("Введiть iдентифiкатор транспортного засобу (формату ХХ-YYYY-XX (де X - будь-яка лiтера англiйського алфавiту у верхньому регiстрi, а Y - будь-яка цифра, наприклад DV-2345-KJ))\n:");
            string id = System.Console.ReadLine();
            System.Console.Write("Виберiть тип транспортного засобу, один з 4х: 0. Легкова(за замовчуванням), 1. Вантажна, 2. Автобус, 3. Мотоцикл\n:");
            int type = 0;
            int.TryParse(System.Console.ReadLine(), out type);
            System.Console.Write("Введiть баланс транспортного засобу\n:");
            decimal sum = 0;
            decimal.TryParse(System.Console.ReadLine(), out sum);

            _parkingService.AddVehicle(new Vehicle(id, (VehicleType)Enum.GetValues<VehicleType>().GetValue(type), sum));
        }

        public static void RemoveVechile()
        {
            System.Console.Write("Введiть iдентифiкатор транспортного засобу (формату ХХ-YYYY-XX (де X - будь-яка лiтера англiйського алфавiту у верхньому регiстрi, а Y - будь-яка цифра, наприклад DV-2345-KJ))\n:");
            string id = System.Console.ReadLine();

            _parkingService.RemoveVehicle(id);
        }

        public static void TopUpVechile()
        {
            System.Console.Write("Введiть iдентифiкатор транспортного засобу (формату ХХ-YYYY-XX (де X - будь-яка лiтера англiйського алфавiту у верхньому регiстрi, а Y - будь-яка цифра, наприклад DV-2345-KJ))\n:");
            string id = System.Console.ReadLine();
            System.Console.Write("Введiть сумму поповнення\n:");
            decimal sum = 0;
            decimal.TryParse(System.Console.ReadLine(), out sum);

            _parkingService.TopUpVehicle(id, sum);
        }
    }
}
