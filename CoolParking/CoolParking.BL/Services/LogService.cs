﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string filePath)
        {
            LogPath = filePath;
        }

        public string LogPath { get; private set; }

        public string Read()
        {
            string textLog;

            if(!File.Exists(LogPath))
            {
                throw new InvalidOperationException("File \"Transactions.log\" not found");
            }

            using (var file = new StreamReader(LogPath))
            {
                textLog = file.ReadToEnd();
            }

            return textLog;
        }

        public void Write(string logInfo)
        {
            using (var file = new StreamWriter(LogPath, true))
            {
                file.WriteLine(logInfo);
            }
        }
    }
}