﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;
        private Parking _parking;
        private List<TransactionInfo> _transactions;
        public DateTime StartWork { get; private set; }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            _logTimer.Elapsed += (_, _) => { LogToFile(); };
            _parking = Parking.GetInstance();
            StartWork = DateTime.Now;
            _transactions = new();
            _withdrawTimer.Start();
            _logTimer.Start();
        }

        public decimal EarnedMoney()
        {
            return _transactions.Sum((t) => t.Sum);
        }
        public List<TransactionInfo> ListOfTransactions()
        {
            return _transactions;
        }
        public List<Vehicle> ListOfVehicles()
        {
            return _parking.GetAll().ToList();
        }

        private void LogToFile()
        {
            foreach (var item in _transactions)
            {
                _logService.Write(item.ToString());
            }
            _transactions.Clear();
            //Console.WriteLine("Writed");
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.GetFreePlaces() == 0)
                throw new ArgumentException("There are no free places in the parking lot");
            if (_parking.GetVehicle(vehicle.Id) != null)
                throw new ArgumentException("The vehicle exists");
            _parking.AddVehicle(vehicle);
            var veh = _parking.GetVehicle(vehicle.Id);
            veh.SetEvent((_,_) => ParkingPayment(veh));
            _withdrawTimer.Elapsed += veh.GetEvent();
        }

        private void ParkingPayment(Vehicle vehicle)
        {
            switch (vehicle.VehicleType)
            {
                case VehicleType.PassengerCar:
                    vehicle.Balance = DebtByType(vehicle.Balance, Settings.TariffOnPassengerCar, vehicle.Id);
                    break;
                case VehicleType.Truck:
                    vehicle.Balance = DebtByType(vehicle.Balance, Settings.TariffOnTruck, vehicle.Id);
                    break;
                case VehicleType.Bus:
                    vehicle.Balance = DebtByType(vehicle.Balance, Settings.TariffOnBus, vehicle.Id);
                    break;
                case VehicleType.Motorcycle:
                    vehicle.Balance = DebtByType(vehicle.Balance, Settings.TariffOnMotorcycle, vehicle.Id);
                    break;
            }
            //Console.WriteLine($"{vehicle.Id} - {vehicle.Balance}");
        }

        private decimal DebtByType(decimal balance, decimal tariff, string id)
        {
            decimal withDraw = tariff;
            if (balance <= 0)
            {
                withDraw = tariff * Settings.Forfeit;
            }
            else if (balance < tariff)
            {
                withDraw = (balance + (tariff - balance) * Settings.Forfeit);
            }
            _parking.TopUpBalance(withDraw);
            _transactions.Add(new TransactionInfo { DateTimeWithDraw = DateTime.Now, VehicleId = id, Sum = withDraw });
            return balance - withDraw;
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return _parking.GetBalance();
        }

        public int GetCapacity()
        {
            return Settings.StartingParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return _parking.GetFreePlaces();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public System.Collections.ObjectModel.ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<Vehicle>(_parking.GetAll());
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.GetVehicle(vehicleId);

            if (vehicle == null)
                throw new ArgumentException("A vehicle with such identifier doesn't exist");
            else if (vehicle.Balance < 0)
                throw new ArgumentException("A vehicle has a negative balance");

            var veh = _parking.GetVehicle(vehicleId);
            _withdrawTimer.Elapsed -= veh.GetEvent();
            _parking.RemoveVehicle(vehicle.Id);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = _parking.GetVehicle(vehicleId);

            if (vehicle == null)
                throw new ArgumentException("A vehicle with such identifier doesn't exist");
            else if (vehicle.Balance < 0)
                throw new ArgumentException("A vehicle has a negative balance");

            if (sum < 0)
                throw new ArgumentException("The sum cannot be negative");
            else if (sum == 0)
                throw new ArgumentException("The sum cannot be zero");

            vehicle.Balance += sum;
        }
    }
}