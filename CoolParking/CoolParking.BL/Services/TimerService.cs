﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get => _timer.Interval; set => _timer.Interval = value; }

        public event ElapsedEventHandler Elapsed;

        private Timer _timer;

        public TimerService(double intervalMilliseconds = 1000)
        {
            _timer = new Timer(intervalMilliseconds);
            Interval = intervalMilliseconds;
            _timer.Elapsed += (s, e) => { Elapsed.Invoke(s, e); };
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        public void Start()
        {
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Enabled = false;
        }
    }
}