﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements
//       of the home task.

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static int StartingParkingBalance { get => 0; }
        public static int StartingParkingCapacity { get => 10; }
        public static int PaymentPeriodSeconds { get => 5; }
        public static int LoggingPeriodSeconds { get => 60; }
        public static decimal TariffOnPassengerCar { get => 2; }
        public static decimal TariffOnTruck { get => 5; }
        public static decimal TariffOnBus { get => 3.5m; }
        public static decimal TariffOnMotorcycle { get => 1; }
        public static decimal Forfeit { get => 2.5m; }
    }
}