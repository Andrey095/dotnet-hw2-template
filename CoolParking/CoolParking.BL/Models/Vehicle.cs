﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;
using System.Timers;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; set; }

        public delegate void Handler(object source, System.Timers.ElapsedEventArgs e);
        public event ElapsedEventHandler handler;

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if(!Regex.IsMatch(id, @"[A-Z]{2}-\d{4}-[A-Z]{2}"))
            {
                throw new ArgumentException("The identifier doesn't match the format XX-YYYY-XX where X is any english upper case letter and Y is an integer");
            }
            Id = id;
            VehicleType = vehicleType;
            if(balance < 0)
            {
                throw new ArgumentException("A vehicle can't have a negative balance");
            }
            Balance = balance;
        }

        public void SetEvent(ElapsedEventHandler _handler)
        {
            handler = _handler;
        }

        public ElapsedEventHandler GetEvent()
        {
            return handler;
        }

        public override string ToString()
        {
            return $"{Id} :: {VehicleType} :: {Balance}";
        }
    }
}