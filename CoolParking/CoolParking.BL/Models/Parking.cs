﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private decimal _balance;
        private List<Vehicle> _parkingLot;

        private Parking()
        {
            _parkingLot = new();
            _balance = Settings.StartingParkingBalance;
        }
        private static Parking _instance;
        public static Parking GetInstance()
        {
            return _instance ??= new();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            _parkingLot.Add(vehicle);
        }
        public IList<Vehicle> GetAll()
        {
            return _parkingLot;
        }
        public Vehicle GetVehicle(string vehicleId)
        {
            return _parkingLot.FirstOrDefault(v => v.Id == vehicleId);
        }
        public Task<Vehicle> GetVehicleAsync(string vehicleId)
        {
            var vehicle = _parkingLot.FirstOrDefault(v => v.Id == vehicleId);
            return new Task<Vehicle>(() => vehicle);
        }
        public Vehicle GetLastVehicle()
        {
            return _parkingLot.LastOrDefault();
        }
        public void RemoveVehicle(string vehicleId)
        {
            _parkingLot.RemoveAll(v => v.Id == vehicleId);
        }
        public int GetFreePlaces()
        {
            return Settings.StartingParkingCapacity - _parkingLot.Count;
        }

        public decimal GetBalance()
        {
            return _balance;
        }
        public void TopUpBalance(decimal sum)
        {
            _balance += sum;
        }
    }
}